package com.kshrd.socketio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    private Socket socket;
    private Button btnSend;
    private EditText etMessage;
    private TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        try {
            socket = IO.socket("http://192.168.178.27:8080");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        socket.on(Socket.EVENT_CONNECT, onConnectEvent());
        socket.on("receive-message", onReceiveMessage());
        socket.connect();

    }

    private Emitter.Listener onReceiveMessage() {
        Emitter.Listener onReceieveMessage = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject jsonObject = (JSONObject) args[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            tvMessage.append(jsonObject.getString("message") + "\n");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        return onReceieveMessage;
    }

    private void initView() {
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        etMessage = (EditText) findViewById(R.id.etMsg);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("message", etMessage.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit("new-message", obj);
            }
        });
    }

    private Emitter.Listener onConnectEvent(){
        Emitter.Listener onConnectEvent = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("ooooo", "Connected");
            }
        };
        return onConnectEvent;
    }
}
